Checkout file.
Make note of where you checked it out to.

Go to SourceTree | Preferences | Custom Actions

Add a new custom action:

# Menu Caption: Bitbucket
# Script to run: <where you checked this file out to>
# Parameters: $SHA $FILE

Note:
It assumes remote URLs have the form:
# git@bitbucket.org/cdacos/sourcetree_bitbucket_custom_action.git
