#!/bin/sh

# SourceTree|Preferences|Custom Actions|Add
# Menu Caption: Bitbucket
# Script to run: <local path to this file>
# Parameters: $SHA $FILE

# Assumes remote URLs of the form:
# git@bitbucket.org/cdacos/sourcetree_bitbucket_custom_action.git
# It does not support the ssh: prefix

function gitrepo_base_url() {
  giturl=$(git config --get remote.origin.url)
  giturl=${giturl/://}
  giturl=${giturl/git\@/}
  giturl=${giturl/\.git/}
  echo $giturl
}

if [ -z "$2" ]; then # We don't have a FILE, so jump to the commit itself
  url="https://$(gitrepo_base_url)/commits/$1"
else
  # open "https://$(gitrepo_base_url)/src/$1/$2" # open the source as it was committed
  url="https://$(gitrepo_base_url)/diff/$2?diff2=$1&at=master" # diff the source from the commit
fi

open $url
